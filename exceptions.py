from trytond.exceptions import UserError

class NonNewbornDOBInput(UserError):
    pass


class CheckWeightAndHeightUOM(UserError):
    pass


class EndDateBeforeStartDate(UserError):
    pass
