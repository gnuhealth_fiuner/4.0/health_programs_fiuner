# -*- coding: utf-8 -*-

from trytond.pool import Pool

from . import health_programs_fiuner

from .report import programs_leche_report
from .report import programs_salud_sexual_report
from .report import programs_remediar_report
from .report import programs_report

from .wizard import wizard_create_programs_reports


def register():
    Pool.register(
        health_programs_fiuner.LecheMaternaMadres,
        health_programs_fiuner.LecheMaternaMenores,
        wizard_create_programs_reports.CreateProgramsReportsStart,
        module='health_programs_fiuner', type_='model')
    Pool.register(
        programs_leche_report.ProgramsLecheReport,
        programs_salud_sexual_report.ProgramsSaludSexualReport,
        programs_remediar_report.ProgramsRemediarReport,
        module='health_programs_fiuner', type_='report')
    Pool.register(
        wizard_create_programs_reports.CreateProgramsReportsWizard,
        module='health_programs_fiuner', type_='wizard')
